package metric

import (
	"bitbucket.org/futonredemption/corelib-go/convert"
	"time"
)

type CounterMetric struct {
	name  string
	count int
	start time.Time
}

func (this *CounterMetric) Increment() {
	this.IncrementBy(1)
}

func (this *CounterMetric) IncrementBy(by int) {
	this.count += by
}

func (this *CounterMetric) GetMetricName() string {
	return this.name
}

func (this *CounterMetric) GetCount() int {
	return this.count
}

func (this *CounterMetric) PerSecond() float32 {
	return this.Per(time.Second)
}

func (this *CounterMetric) Per(duration time.Duration) float32 {
	now := time.Now()
	dur := now.Sub(this.start)

	return float32(this.count) / float32(dur/duration)
}

func (this *CounterMetric) Reset() {
	this.count = 0
	this.start = time.Now()
}

func (this *CounterMetric) String() string {
	return this.name + " Count: " + convert.IntToString(this.count) + " /second: " + convert.Float32ToString(this.PerSecond())
}

func NewCounterMetric(name string) *CounterMetric {
	return &CounterMetric{
		count: 0,
		name:  name,
		start: time.Now(),
	}
}
