package metric

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

type testPrinter struct {
	ticks int
}

func (this *testPrinter) PrintMetrics(metrics []Metric) {
	for _ = range metrics {
		this.ticks++
	}
}

func TestPollerTick(t *testing.T) {
	assert := assert.New(t)
	p := &testPrinter{}
	pollprinter := NewPollPrinterWithPrinter(p)
	pollprinter.Add(NewCounterMetric("one"), NewCounterMetric("two"))
	pollprinter.Start()
	time.Sleep(1500 * time.Millisecond)
	pollprinter.Stop()
	assert.Equal(p.ticks, 2, "ticks")
}
