package metric

// Metric is the standard interface for all metrics.
type Metric interface {
	// Gets the name of the metric.
	GetMetricName() string
	// Resets the metric.
	Reset()
	// Gets stringified metric.
	String() string
}
