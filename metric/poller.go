package metric

import (
	"bitbucket.org/futonredemption/corelib-go/scheduling"
	"log"
)

// Printer prints metrics.
type Printer interface {
	PrintMetrics(metrics []Metric)
}

type logPrinter struct{}

func (this *logPrinter) PrintMetrics(metrics []Metric) {
	for _, m := range metrics {
		log.Println(m.String())
	}
}

// PollPrinter prints all registered metrics at a specific interval.
type PollPrinter struct {
	metrics []Metric
	poller  scheduling.Poller
	printer Printer
}

// Start printing metrics.
func (this *PollPrinter) Start() {
	this.poller.StartPolling()
}

// Stop printing metrics.
func (this *PollPrinter) Stop() {
	this.poller.StopPolling()
}

// Adds a metric to be printed.
func (this *PollPrinter) Add(mList ...Metric) {
	for _, m := range mList {
		this.metrics = append(this.metrics, m)
	}
}

// Prints the metrics.
func (this *PollPrinter) Run() {
	this.printer.PrintMetrics(this.metrics)
	for _, m := range this.metrics {
		log.Println(m.String())
	}
}

// NewPollPrinter creates a new PollPrinter.
func NewPollPrinter() *PollPrinter {
	return NewPollPrinterWithPrinter(&logPrinter{})
}

// NewPollPrinterWithPrinter creates a new PollPrinter with a custom printer.
func NewPollPrinterWithPrinter(printer Printer) *PollPrinter {
	poller := &PollPrinter{
		printer: printer,
	}
	poller.poller = scheduling.NewPoller(1, poller)
	return poller
}
