package metric

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

const (
	COUNTER_NAME = "/testing/test-counter"
)

func TestNewCounterMetric(t *testing.T) {
	assert := assert.New(t)

	counter := NewCounterMetric(COUNTER_NAME)

	assert.Equal(COUNTER_NAME, counter.GetMetricName())
	assert.Equal(0, counter.GetCount())
}

func TestCounterMetricIsMetric(t *testing.T) {
	assert := assert.New(t)

	counter := (Metric)(NewCounterMetric(COUNTER_NAME))
	assert.NotNil(counter)
}

func TestCounterReset(t *testing.T) {
	assert := assert.New(t)

	counter := NewCounterMetric(COUNTER_NAME)
	counter.Increment()
	counter.Reset()
	assert.Equal(0, counter.GetCount())
}

func TestCounterIncrement(t *testing.T) {
	assert := assert.New(t)

	counter := NewCounterMetric(COUNTER_NAME)
	counter.Increment()
	assert.Equal(1, counter.GetCount())
}

func TestCounterIncrementBy(t *testing.T) {
	assert := assert.New(t)

	counter := NewCounterMetric(COUNTER_NAME)
	counter.IncrementBy(50)
	assert.Equal(50, counter.GetCount())
}

func TestCounterPerSecond(t *testing.T) {
	assert := assert.New(t)

	counter := NewCounterMetric(COUNTER_NAME)
	counter.IncrementBy(60)
	counter.start = time.Now().Add((time.Duration)(-1 * time.Minute))
	assert.InEpsilon(1.0, counter.PerSecond(), 0.0001)
	assert.InEpsilon(0.001, counter.Per(time.Millisecond), 0.0001)
	assert.InEpsilon(60.0, counter.Per(time.Minute), 0.0001)
}

func TestCounterToString(t *testing.T) {
	assert := assert.New(t)

	counter := NewCounterMetric(COUNTER_NAME)
	counter.IncrementBy(60)
	counter.start = time.Now().Add((time.Duration)(-6 * time.Second))
	assert.Equal("/testing/test-counter Count: 60 /second: 10", counter.String())
}
