package convert

import (
	"bytes"
	"encoding/binary"
	"strconv"
)

// Int64ToBytes converts int64 to []byte.
func Int64ToBytes(v int64) []byte {
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(v))
	return b
}

// BytesToInt64 converts []byte to int64.
func BytesToInt64(v []byte) (int64, error) {
	var result int64
	buf := bytes.NewReader(v)
	err := binary.Read(buf, binary.LittleEndian, &result)
	return result, err
}

// Float64ToBytes converts float64 to []byte.
func Float64ToBytes(v float64) ([]byte, error) {
	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, v)
	return buf.Bytes(), err
}

// BytesToFloat64 converts []byte to float64.
func BytesToFloat64(v []byte) (float64, error) {
	var result float64
	buf := bytes.NewReader(v)
	err := binary.Read(buf, binary.LittleEndian, &result)
	return result, err
}

// IntToString converts int to string.
func IntToString(v int) string {
	return strconv.Itoa(v)
}

// StringToInt converts string to int.
func StringToInt(v string) (int, error) {
	return strconv.Atoi(v)
}

// Float32ToString converts float32 to string.
func Float32ToString(v float32) string {
	return strconv.FormatFloat(float64(v), 'f', -1, 32)
}

// StringToFloat32 converts string to float32.
func StringToFloat32(v string) (float32, error) {
	r, err := strconv.ParseFloat(v, 32)
	return float32(r), err
}

// Float64ToString converts float64 to string.
func Float64ToString(v float64) string {
	return strconv.FormatFloat(v, 'f', -1, 64)
}

// StringToFloat64 converts string to float64.
func StringToFloat64(v string) (float64, error) {
	return strconv.ParseFloat(v, 64)
}
