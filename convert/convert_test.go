package convert

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConvertInt64(t *testing.T) {
	assert := assert.New(t)

	var valuesToTest = []struct {
		in int64
	}{
		{0},
		{1},
		{1000},
		{1024},
		{-1},
		{-1024},
	}
	for _, value := range valuesToTest {
		byteValue := Int64ToBytes(value.in)
		convertedBackValue, err := BytesToInt64(byteValue)
		assert.Nil(err)
		assert.Equal(value.in, convertedBackValue)
	}
}

func TestConvertFloat64Bytes(t *testing.T) {
	assert := assert.New(t)

	var valuesToTest = []struct {
		in float64
	}{
		{0.0},
		{1.5},
		{1000.12345},
		{1024.09180923},
		{-1024.09180923},
		{99999999999999974834176},
		{100000000000000016777215},
	}
	for _, value := range valuesToTest {
		byteValue, err := Float64ToBytes(value.in)
		assert.Nil(err)
		convertedBackValue, err := BytesToFloat64(byteValue)
		assert.Nil(err)
		assert.Equal(value.in, convertedBackValue)
	}
}

func TestConvertFloat32String(t *testing.T) {
	assert := assert.New(t)

	var valuesToTest = []struct {
		in  float32
		str string
	}{
		{0.0, "0"},
		{1.5, "1.5"},
		{1000.12345, "1000.12345"},
		{1024.09180923, "1024.09180923"},
		{-1024.09180923, "-1024.09180923"},
		{99999999999999974834176, "99999999999999974834176"},
		{100000000000000016777215, "100000000000000016777215"},
	}
	for _, value := range valuesToTest {
		stringValue := Float32ToString(value.in)
		//assert.Equal(value.str, stringValue, "String conversion lost precision.")
		convertedBackValue, err := StringToFloat32(stringValue)
		assert.Nil(err)
		assert.Equal(value.in, convertedBackValue, "Lost precision on BACK conversion.")
	}
}

func TestConvertFloat64String(t *testing.T) {
	assert := assert.New(t)

	var valuesToTest = []struct {
		in  float64
		str string
	}{
		{0.0, "0"},
		{1.5, "1.5"},
		{1000.12345, "1000.12345"},
		{1024.09180923, "1024.09180923"},
		{-1024.09180923, "-1024.09180923"},
		{99999999999999974834176, "99999999999999974834176"},
		{100000000000000016777215, "100000000000000016777215"},
	}
	for _, value := range valuesToTest {
		stringValue := Float64ToString(value.in)
		//assert.Equal(value.str, stringValue, "String conversion lost precision.")
		convertedBackValue, err := StringToFloat64(stringValue)
		assert.Nil(err)
		assert.Equal(value.in, convertedBackValue, "Lost precision on BACK conversion.")
	}
}

func TestConvertStringInt(t *testing.T) {
	assert := assert.New(t)

	var valuesToTest = []struct {
		in int
	}{
		{0},
		{1},
		{1000},
		{1024},
		{-1},
		{-1024},
	}
	for _, value := range valuesToTest {
		stringValue := IntToString(value.in)
		convertedBackValue, err := StringToInt(stringValue)
		assert.Nil(err)
		assert.Equal(value.in, convertedBackValue)
	}
}

func ExampleInt64ToBytes() {
	fmt.Printf("%v", Int64ToBytes(1024))
	// Output: [0 4 0 0 0 0 0 0]
}

func ExampleFloat64ToBytes() {
	byteData, _ := Float64ToBytes(1024.1024)
	fmt.Printf("%v", byteData)
	// Output: [113 172 139 219 104 0 144 64]
}

func BenchmarkInt64ToBytes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Int64ToBytes(int64(i))
	}
}

func BenchmarkFloat64ToBytes(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Float64ToBytes(float64(i))
	}
}
