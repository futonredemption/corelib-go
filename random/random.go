package random

import (
	"bitbucket.org/futonredemption/corelib-go/convert"
	"crypto/rand"
	"log"
)

// Gets a random int64 value.
func Int64() int64 {
	b := make([]byte, 8)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatalf("random.Int64() %s", err)
	}
	r, err := convert.BytesToInt64(b)
	if err != nil {
		log.Fatalf("random.Int64(), convert.BytesToInt64, %s", err)
	}
	return r
}
