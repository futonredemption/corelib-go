package random

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestInt64(t *testing.T) {
	assert := assert.New(t)
	for i := 0; i < 500; i++ {
		assert.NotEqual(Int64(), Int64())
		fmt.Println(Int64())
	}
}

func BenchmarkInt64(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Int64()
	}
}
