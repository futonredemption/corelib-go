package jsonfile

import (
	"bitbucket.org/futonredemption/corelib-go/filesystem"
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type ExampleData struct {
	DataString string
	DataInt    int
}

const JSON_TEMPLATE string = `{
  "DataString": "%s",
  "DataInt": %d
}`

func TestNewJsonFile_Empty(t *testing.T) {
	assert := assert.New(t)

	assert.False(filesystem.Exists("data.json"))
	jsonFile := NewJsonFile("data.json", &ExampleData{})
	jsonString, err := jsonFile.ToJsonString()
	assert.Nil(err)
	assert.Equal(fmt.Sprintf(JSON_TEMPLATE, "", 0), jsonString, "Load should succeed even if file does not exist.")
}

func TestLoad_FileDoesNotExist(t *testing.T) {
	assert := assert.New(t)

	assert.False(filesystem.Exists("data.json"))
	jsonFile := NewJsonFile("data.json", &ExampleData{})
	assert.Nil(jsonFile.Load(), "Load should succeed even if file does not exist.")
	jsonString, err := jsonFile.ToJsonString()
	assert.Nil(err)
	assert.Equal(fmt.Sprintf(JSON_TEMPLATE, "", 0), jsonString, "Expected content should match.")
}

func TestLoad_FileExists(t *testing.T) {
	assert := assert.New(t)

	jsonData := fmt.Sprintf(JSON_TEMPLATE, "data", 9000)
	f, err := filesystem.CreateTempFileFromString(jsonData)
	dataFile := f.Name()
	assert.Nil(err, "Write file should work.")
	assert.True(filesystem.Exists(dataFile))
	jsonFile := NewJsonFile(dataFile, &ExampleData{})
	assert.Nil(jsonFile.Load(), "Load should succeed even if file does not exist.")
	jsonString, err := jsonFile.ToJsonString()
	assert.Nil(err)
	assert.Equal(jsonData, jsonString, "JSON data from file should match.")

	filesystem.DeleteFile(dataFile)
	assert.False(filesystem.Exists(dataFile))
}

func TestSave_DataChanged(t *testing.T) {
	assert := assert.New(t)

	f, err := filesystem.CreateTempFileFromString("{}")
	dataFile := f.Name()

	assert.True(filesystem.Exists(dataFile))
	jsonFile := NewJsonFile(dataFile, &ExampleData{})

	exampleData := jsonFile.GetData().(*ExampleData)
	exampleData.DataString = "data-string"
	exampleData.DataInt = 10000
	assert.Nil(jsonFile.Save(), "Save should succeed.")
	dataString, err := filesystem.ReadStringFromFile(dataFile)
	assert.Nil(err)

	assert.Equal(fmt.Sprintf(JSON_TEMPLATE, "data-string", 10000), dataString, "Expected content should match.")

	filesystem.DeleteFile(dataFile)
	assert.False(filesystem.Exists(dataFile))
}

func TestLoadFromBytes(t *testing.T) {
	assert := assert.New(t)

	data := &ExampleData{}
	err := LoadFromBytes(data, []byte(fmt.Sprintf(JSON_TEMPLATE, "loaded", 9000)))
	assert.Nil(err)
	assert.Equal("loaded", data.DataString)
	assert.Equal(9000, data.DataInt)
}

func TestToJsonBytes(t *testing.T) {
	assert := assert.New(t)

	data := &ExampleData{
		DataString: "loaded",
		DataInt:    9000,
	}
	jsonBytes, err := ToJsonBytes(data)
	assert.Nil(err)
	assert.Equal([]byte(fmt.Sprintf(JSON_TEMPLATE, "loaded", 9000)), jsonBytes)
}

func TestLoadFromBytesAndToJsonBytes(t *testing.T) {
	assert := assert.New(t)

	data := &ExampleData{
		DataString: "loaded",
		DataInt:    9000,
	}
	jsonBytes, err := ToJsonBytes(data)
	assert.Nil(err)

	newData := &ExampleData{}
	err = LoadFromBytes(newData, jsonBytes)
	assert.Nil(err)
	assert.Equal(data.DataString, newData.DataString)
	assert.Equal(data.DataInt, newData.DataInt)
}

func ExampleNewJsonFile() {
	jsonFile := NewJsonFile("data.json", &ExampleData{DataString: "data"})
	data, _ := jsonFile.ToJsonString()
	fmt.Printf(data)
	// Output: {
	//   "DataString": "data",
	//   "DataInt": 0
	// }
}

func ExampleGetData() {
	jsonFile := NewJsonFile("data.json", &ExampleData{DataString: "data"})

	exampleData := jsonFile.GetData().(*ExampleData)
	fmt.Printf("%s %d", exampleData.DataString, exampleData.DataInt)
	// Output: data 0
}
