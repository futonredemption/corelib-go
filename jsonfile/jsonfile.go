package jsonfile

import (
	"bitbucket.org/futonredemption/corelib-go/filesystem"
	"encoding/json"
	"os"
)

// A file backed by JSON data.
type JsonFile interface {
	// Loads contents from the JSON file.
	Load() error
	// Loads JSON data from a string.
	LoadFromString(jsonString string) error
	// Loads JSON data from a byte array.
	LoadFromBytes(jsonBytes []byte) error
	// Saves the JSON data to the file.
	Save() error
	// Converts internal data structure to JSON formatted string.
	ToJsonString() (string, error)
	// Converts internal data structure to a JSON formatted byte array.
	ToJsonBytes() ([]byte, error)
	// Gets the data that is backed by the JSON file.
	// The type of the object will match the initial empty data object that was passed in.
	GetData() interface{}
}

type v1JsonFile struct {
	filePath string
	fileData interface{}
}

func (this *v1JsonFile) Load() error {
	contents, err := filesystem.ReadBytesFromFile(this.filePath)
	if os.IsNotExist(err) {
		return this.LoadFromString("{}")
	} else if err != nil {
		return err
	}
	return this.LoadFromBytes(contents)
}

func (this *v1JsonFile) LoadFromString(jsonString string) error {
	return this.LoadFromBytes([]byte(jsonString))
}

func (this *v1JsonFile) LoadFromBytes(jsonBytes []byte) error {
	return json.Unmarshal(jsonBytes, &this.fileData)
}

func (this *v1JsonFile) Save() error {
	data, err := this.ToJsonBytes()
	if err != nil {
		return err
	}
	return filesystem.WriteBytesToFile(this.filePath, data)
}

func (this *v1JsonFile) ToJsonString() (string, error) {
	data, err := this.ToJsonBytes()
	return string(data), err
}

func (this *v1JsonFile) ToJsonBytes() ([]byte, error) {
	return json.MarshalIndent(this.fileData, "", "  ")
}

func (this *v1JsonFile) GetData() interface{} {
	return this.fileData
}

func NewJsonFile(path string, emptyData interface{}) JsonFile {
	return &v1JsonFile{
		filePath: path,
		fileData: emptyData,
	}
}

// Unserializes JSON encoding in byte string to the data structure.
func LoadFromBytes(data interface{}, jsonBytes []byte) error {
	return json.Unmarshal(jsonBytes, &data)
}

// Serializes data structure into a JSON encoded byte string.
func ToJsonBytes(data interface{}) ([]byte, error) {
	return json.MarshalIndent(data, "", "  ")
}
