package stringutil

import (
	"bitbucket.org/futonredemption/corelib-go/random"
)

const ENGLISH_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const ENGLISH_ALPHANUMERIC = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// Creates a random string using only letters from the English alphabet.
func RandomAlphaString(size int) string {
	return RandomStringWithAlphabet(size, ENGLISH_ALPHABET)
}

// Creates a random string using letters from the English alphabet and numbers.
func RandomAlphaNumericString(size int) string {
	return RandomStringWithAlphabet(size, ENGLISH_ALPHANUMERIC)
}

// Create a random string using an arbitary alphabet string.
func RandomStringWithAlphabet(size int, alphabet string) string {
	// Based on http://play.golang.org/p/WIgH7GRnN1
	b := make([]byte, size)
	// A src.Int64() generates 64 random bits, enough for letterIdxMax characters!
	for i, cache, remain := size-1, random.Int64(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = random.Int64(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(alphabet) {
			b[i] = alphabet[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
