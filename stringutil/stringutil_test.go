package stringutil

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestRandomAlphaString(t *testing.T) {
	assert := assert.New(t)

	assert.NotEqual(RandomAlphaString(10), RandomAlphaString(10))
	assert.Equal(10, len(RandomAlphaString(10)))
}

func TestRandomAlphaNumericString(t *testing.T) {
	assert := assert.New(t)

	assert.NotEqual(RandomAlphaNumericString(10), RandomAlphaNumericString(10))
	assert.Equal(10, len(RandomAlphaNumericString(10)))
}

func TestRandomStringWithAlphabet(t *testing.T) {
	assert := assert.New(t)

	lhs := RandomStringWithAlphabet(10, "a")
	rhs := RandomStringWithAlphabet(10, "a")
	assert.Equal(lhs, rhs, "LHS: \""+lhs+"\" RHS: \""+rhs+"\" should be the same string, all 'a's.")
	lhs = RandomStringWithAlphabet(10, "abc")
	rhs = RandomStringWithAlphabet(10, "abc")
	assert.NotEqual(lhs, rhs, "LHS: \""+lhs+"\" RHS: \""+rhs+"\" should NOT be the same string, mix of {a,b,c}'s.")
	assert.Equal(10, len(RandomAlphaString(10)))
}

func BenchmarkRandomAlphaString32(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RandomAlphaString(32)
	}
}

func BenchmarkRandomAlphaString256(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RandomAlphaString(256)
	}
}

func BenchmarkRandomAlphaString1024(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RandomAlphaString(1024)
	}
}

func BenchmarkRandomAlphaString32768(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RandomAlphaString(32768)
	}
}
