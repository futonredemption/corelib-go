package filesystem

import (
	"bitbucket.org/futonredemption/corelib-go/stringutil"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
)

// Creates a new directory.
// This method will create all directories necessary in the path.
func CreateDirectory(path string) error {
	return os.MkdirAll(path, os.FileMode(0777))
}

// Deletes the directory and all of it's contents.
func DeleteDirectory(path string) error {
	return os.RemoveAll(path)
}

// Returns true if the path specified is a directory.
func IsDirectory(path string) bool {
	dirStat, err := os.Stat(path)
	if err != nil {
		return false
	}
	return dirStat.IsDir()
}

// Returns true if the file/directory exists.
func Exists(path string) bool {
	_, err := os.Stat(path)
	return !os.IsNotExist(err)
}

// Create a random temporary file.
func CreateTempFile() (*os.File, error) {
	return CreateTempFileWithBaseDirAndPrefix(os.TempDir(), "tmp-")
}

// Creates a temporary file that contains the contentString.
func CreateTempFileFromString(contentString string) (*os.File, error) {
	return CreateTempFileFromBytes([]byte(contentString))
}

// Creates a temporary file that contains the content bytes.
func CreateTempFileFromBytes(content []byte) (*os.File, error) {
	f, err := CreateTempFile()
	if err != nil {
		return nil, err
	}
	return f, WriteBytesToFile(f.Name(), content)
}

// Creates a temporary file with a specific prefix.
func CreateTempFileWithName(prefix string) (*os.File, error) {
	return CreateTempFileWithBaseDirAndPrefix(os.TempDir(), prefix)
}

// Creates a temporary file in a specific directory and specific prefix.
func CreateTempFileWithBaseDirAndPrefix(path, prefix string) (*os.File, error) {
	return ioutil.TempFile(path, prefix)
}

// Creates a random temporary directory.
func RandomTempDirectory() (string, error) {
	return RandomTempDirectoryWithBaseDirAndPrefix(os.TempDir(), "tmp-")
}

// Creates a random temporary directory in a specific base directory with a prefix.
func RandomTempDirectoryWithBaseDirAndPrefix(baseTempDir string, prefix string) (string, error) {
	for i := 0; i < 100; i += 1 {
		candidatePath := filepath.Join(baseTempDir, prefix+stringutil.RandomAlphaNumericString(10))
		if !Exists(candidatePath) {
			err := CreateDirectory(candidatePath)
			if err == nil {
				return candidatePath, nil
			}
		}
	}
	return os.TempDir(), errors.New("Cannot create a random directory")
}

// Reads file and returns contents as a string.
func ReadStringFromFile(filePath string) (string, error) {
	bytes, err := ReadBytesFromFile(filePath)
	return string(bytes), err
}

// Reads file and returns contents as a byte array.
func ReadBytesFromFile(filePath string) ([]byte, error) {
	return ioutil.ReadFile(filePath)
}

// Writes string data to a file.
func WriteStringToFile(filePath string, contents string) error {
	return WriteBytesToFile(filePath, []byte(contents))
}

// Write byte array to a file.
func WriteBytesToFile(filePath string, contents []byte) error {
	return ioutil.WriteFile(filePath, contents, 0660)
}

// Deletes a file
func DeleteFile(path string) error {
	return os.Remove(path)
}

// Opens a file for writing.
// If the file does not exist it will be created,
// otherwise the file's old contents are overwritten.
func OpenFileForWrite(path string) (*os.File, error) {
	return os.Create(path)
}
