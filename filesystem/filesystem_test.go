package filesystem

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const (
	TEST_FILE             = "test-file"
	TEST_FILE_DATA_STRING = "test file data"
)

var TEST_FILE_DATA = []byte(TEST_FILE_DATA_STRING)

func TestWriteStringToFile(t *testing.T) {
	assert := assert.New(t)

	assert.False(Exists(TEST_FILE))
	err := WriteStringToFile(TEST_FILE, TEST_FILE_DATA_STRING)
	assert.True(Exists(TEST_FILE))
	assert.Nil(err)
	contentString, err := ReadStringFromFile(TEST_FILE)
	assert.Nil(err)
	assert.Equal(TEST_FILE_DATA_STRING, contentString)
	DeleteFile(TEST_FILE)
	assert.False(Exists(TEST_FILE))
}

func TestOpenFileForWrite(t *testing.T) {
	assert := assert.New(t)

	assert.False(Exists(TEST_FILE))
	fp, err := OpenFileForWrite(TEST_FILE)
	assert.True(Exists(TEST_FILE))
	assert.Nil(err)
	fp.Write(TEST_FILE_DATA)
	fp.Close()
	readBytes, err := ReadBytesFromFile(TEST_FILE)
	assert.Nil(err)
	assert.Equal(TEST_FILE_DATA, readBytes)
	DeleteFile(TEST_FILE)
	assert.False(Exists(TEST_FILE))
}

func TestRandomDirectory(t *testing.T) {
	assert := assert.New(t)

	dir, err := RandomTempDirectory()
	assert.Nil(err)
	assert.True(IsDirectory(dir))
	assert.True(Exists(dir))

	err = DeleteDirectory(dir)
	assert.Nil(err)
	assert.False(IsDirectory(dir))
	assert.False(Exists(dir))
}

func BenchmarkRandomTempDirectory(b *testing.B) {
	for i := 0; i < b.N; i++ {
		dir, _ := RandomTempDirectory()
		DeleteDirectory(dir)
	}
}
