package scheduling

import (
	"time"
)

type Poller interface {
	StartPolling()
	StopPolling()
}

type Runner interface {
	Run()
}

type v1Poller struct {
	intervalSeconds int
	tickerChannel   chan struct{}
	runner          Runner
}

func (this *v1Poller) StartPolling() {
	ticker := time.NewTicker(time.Duration(this.intervalSeconds) * time.Second)
	this.tickerChannel = make(chan struct{})
	go func() {
		for {
			select {
			case <-ticker.C:
				this.runner.Run()
			case <-this.tickerChannel:
				ticker.Stop()
				return
			}
		}
	}()
}

func (this *v1Poller) StopPolling() {
	if this.tickerChannel != nil {
		close(this.tickerChannel)
	}
}

func NewPoller(intervalSeconds int, runner Runner) Poller {
	return &v1Poller{
		intervalSeconds: intervalSeconds,
		runner:          runner,
	}
}
