package scheduling

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

type ExampleRunner struct {
	ticks int
}

func (this *ExampleRunner) Run() {
	this.ticks += 1
}

func TestPollerTick(t *testing.T) {
	assert := assert.New(t)

	runner := &ExampleRunner{}
	poller := NewPoller(1, runner)
	poller.StartPolling()
	time.Sleep(1500 * time.Millisecond)
	poller.StopPolling()
	assert.Equal(runner.ticks, 1, "runner.ticks")
}

func ExampleStartPolling() {
	runner := &ExampleRunner{}
	poller := NewPoller(1, runner)
	poller.StartPolling()
	time.Sleep(1500 * time.Millisecond)
	poller.StopPolling()
	fmt.Printf("Ticks: %d", runner.ticks)
	// Output: Ticks: 1
}
