// +build !appengine

package datastore

import (
	"bitbucket.org/futonredemption/corelib-go/filesystem"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOpenClose(t *testing.T) {
	assert := assert.New(t)

	fp, err := filesystem.CreateTempFile()
	filePath := fp.Name()

	ds := NewBoltDatastore()
	assert.Nil(err)
	assert.Nil(ds.Open(filePath))
	assert.Nil(ds.Close())
	assert.Nil(filesystem.DeleteFile(filePath))
}

func TestCreateBucketDeleteBucket(t *testing.T) {
	assert := assert.New(t)

	fp, err := filesystem.CreateTempFile()
	filePath := fp.Name()

	ds := NewBoltDatastore()
	assert.Nil(err)
	assert.Nil(ds.Open(filePath))
	assert.Nil(ds.CreateBucket("bucket"))
	assert.Nil(ds.CreateBucket("bucket"))
	assert.Nil(ds.DeleteBucket("bucket"))
	assert.Equal("bucket not found", ds.DeleteBucket("bucket").Error())
	assert.Nil(ds.Close())
	assert.Nil(filesystem.DeleteFile(filePath))
}
