// +build !appengine

package datastore

import (
	"github.com/boltdb/bolt"
	"time"
)

type Datastore interface {
	// Opens a datastore, if it does not exist a new datastore is created.
	Open(fileName string) error
	// Closes the database and commits all data.
	Close() error
	// Creates a bucket of data if it does not exist.
	CreateBucket(bucketName string) error
	// Deletes a bucket of data if it does exist.
	DeleteBucket(bucketName string) error
}

type v1BoltDatastore struct {
	db *bolt.DB
}

func (this *v1BoltDatastore) Open(fileName string) error {
	db, err := bolt.Open(fileName, 0600, &bolt.Options{Timeout: 1 * time.Second})
	this.db = db
	return err
}

func (this *v1BoltDatastore) Close() error {
	if this.db == nil {
		return nil
	}

	err := this.db.Close()
	if err == nil {
		this.db = nil
	}
	return err
}

func (this *v1BoltDatastore) CreateBucket(bucketName string) error {
	return this.db.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		return err
	})
}

func (this *v1BoltDatastore) DeleteBucket(bucketName string) error {
	return this.db.Update(func(tx *bolt.Tx) error {
		return tx.DeleteBucket([]byte(bucketName))
	})
}

func NewBoltDatastore() Datastore {
	return &v1BoltDatastore{}
}
