// +build appengine

package httpwrapper

import (
	"google.golang.org/appengine/aetest"
	"google.golang.org/appengine/urlfetch"
)

func createHttpWrapperForTest() (HttpWrapper, func()) {
	context, future, _ := aetest.NewContext()
	return New(urlfetch.Client(context)), future
}
