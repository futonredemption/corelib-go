package httpwrapper

import (
	"bitbucket.org/futonredemption/corelib-go/filesystem"
	"io"
	"io/ioutil"
	"net/http"
)

// Helper class for handling common HTTP operations.
type HttpWrapper interface {
	// Downloads a file from a public HTTP server.
	GetFileContents(url string) ([]byte, error)
	// Downloads a file from a public HTTP server to a local file.
	DownloadFile(url string, localFilePath string) (bytesWritten int64, err error)
}

type httpWrapperImpl struct {
	client *http.Client
}

func (this *httpWrapperImpl) GetFileContents(url string) ([]byte, error) {
	resp, err := this.client.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return data, nil
}

func (this *httpWrapperImpl) DownloadFile(url string, localFilePath string) (bytesWritten int64, err error) {
	resp, err := this.client.Get(url)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	fp, err := filesystem.OpenFileForWrite(localFilePath)
	if err != nil {
		return 0, nil
	}
	defer fp.Close()

	return io.Copy(fp, resp.Body)
}

func NewDefault() HttpWrapper {
	return New(&http.Client{})
}

func New(client *http.Client) HttpWrapper {
	return &httpWrapperImpl{
		client: client,
	}
}

func DownloadFile(url string, localFilePath string) (bytesWritten int64, err error) {
	return NewDefault().DownloadFile(url, localFilePath)
}
