// +build !appengine

package httpwrapper

import (
	"bitbucket.org/futonredemption/corelib-go/crypto"
	"bitbucket.org/futonredemption/corelib-go/filesystem"
	"errors"
	"log"
	"net/http"
	"strconv"
)

// Represents an HTTP/HTTPS server.
type HttpServer interface {
	// Sets the port of the HTTP server. If not set then HTTP will not be available.
	SetHttpPort(port int) HttpServer
	// Sets the port of the HTTPS server. If not set then HTTPS will not be available.
	SetHttpsPort(port int) HttpServer
	// Sets the handler that will govern how HTTP/HTTPS traffic is served.
	SetHandler(httpHandler http.Handler) HttpServer
	// Set the HTTP handler as a file server.
	SetFileServer(dir string) HttpServer
	// Sets the certificate and private key path for HTTPS. If not set then keys will be automatically generated.
	SetHttpsCertificateAndKey(certPath string, privateKeyPath string) HttpServer
	IsHttpEnabled() bool
	IsHttpsEnabled() bool
	ServeAsync() error
	ServeAndWait() error
}

type httpServerImpl struct {
	httpPort       string
	httpsPort      string
	httpHandler    http.Handler
	certPath       string
	privateKeyPath string
}

func (this *httpServerImpl) SetHttpPort(port int) HttpServer {
	this.httpPort = ":" + strconv.Itoa(port)
	return this
}

func (this *httpServerImpl) SetHttpsPort(port int) HttpServer {
	this.httpsPort = ":" + strconv.Itoa(port)
	return this
}

func (this *httpServerImpl) SetHttpsCertificateAndKey(certPath string, privateKeyPath string) HttpServer {
	this.certPath = certPath
	this.privateKeyPath = privateKeyPath
	return this
}

func (this *httpServerImpl) SetFileServer(dir string) HttpServer {
	return this.SetHandler(http.FileServer(http.Dir(dir + "/")))
}

func (this *httpServerImpl) SetHandler(handler http.Handler) HttpServer {
	this.httpHandler = handler
	return this
}

func (this *httpServerImpl) IsHttpEnabled() bool {
	return this.httpPort != "" && this.httpHandler != nil
}

func (this *httpServerImpl) IsHttpsEnabled() bool {
	return this.httpsPort != "" && this.httpHandler != nil
}

func (this *httpServerImpl) setupHttpsKeys() error {
	if this.IsHttpsEnabled() && this.certPath == "" {
		certFp, err := filesystem.CreateTempFileWithName("https-cert")
		if err != nil {
			return err
		}
		privateKeyFp, err := filesystem.CreateTempFileWithName("https-private-key")
		if err != nil {
			return err
		}

		this.certPath = certFp.Name()
		this.privateKeyPath = privateKeyFp.Name()

		certBuilder := crypto.NewCertificateBuilder()
		err = certBuilder.WriteCertificate(this.certPath)
		if err != nil {
			return err
		}
		return certBuilder.WritePrivateKey(this.privateKeyPath)
	}
	return nil
}

func (this *httpServerImpl) ServeAsync() error {
	if !this.IsHttpsEnabled() && !this.IsHttpEnabled() {
		return errors.New("Neither HTTP or HTTPS is configured. Make sure that a port and handler is set.")
	}
	err := this.setupHttpsKeys()
	if err != nil {
		return err
	}
	go func() {
		err = http.ListenAndServeTLS(this.httpsPort, this.certPath, this.privateKeyPath, this.httpHandler)
		if err != nil {
			log.Fatal(err)
		}
	}()
	go func() {
		err = http.ListenAndServe(this.httpPort, this.httpHandler)
		if err != nil {
			log.Fatal(err)
		}
	}()
	return nil
}

func (this *httpServerImpl) ServeAndWait() error {
	err := this.ServeAsync()
	if err != nil {
		return err
	}
	ch := make(chan bool)
	<-ch
	return nil
}

// Create a new http server.
func NewHttpServer() HttpServer {
	return &httpServerImpl{
		httpPort:       "",
		httpsPort:      "",
		httpHandler:    nil,
		certPath:       "",
		privateKeyPath: "",
	}
}
