// +build !appengine

package httpwrapper

func createHttpWrapperForTest() (HttpWrapper, func()) {
	return NewDefault(), func() {}
}
