package httpwrapper

import (
	"bitbucket.org/futonredemption/corelib-go/filesystem"
	"github.com/stretchr/testify/assert"
	"testing"
)

var TEST_FILE_DATA = []byte("test file")

const (
	PUBLIC_TEST_FILE = "https://storage.googleapis.com/faceshift-cloud.appspot.com/test/testfile"
)

func TestGetFileContents(t *testing.T) {
	assert := assert.New(t)

	httpWrapper, future := createHttpWrapperForTest()
	defer future()

	downloadedData, err := httpWrapper.GetFileContents(PUBLIC_TEST_FILE)
	assert.Nil(err)
	assert.Equal(TEST_FILE_DATA, downloadedData)
}

func TestDownloadFile(t *testing.T) {
	assert := assert.New(t)

	fp, err := filesystem.CreateTempFile()
	assert.Nil(err)
	fp.Close()

	httpWrapper, future := createHttpWrapperForTest()
	defer future()

	bytesWritten, err := httpWrapper.DownloadFile(PUBLIC_TEST_FILE, fp.Name())
	assert.Nil(err)
	assert.Equal(len(TEST_FILE_DATA), int(bytesWritten))
	data, err := filesystem.ReadBytesFromFile(fp.Name())
	assert.Nil(err)
	assert.Equal(TEST_FILE_DATA, data)
	filesystem.DeleteFile(fp.Name())
	assert.False(filesystem.Exists(fp.Name()))
}
