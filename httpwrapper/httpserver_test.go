// +build !appengine

package httpwrapper

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewHttpServer(t *testing.T) {
	assert := assert.New(t)

	httpServer := NewHttpServer()
	assert.NotNil(httpServer)
}

func Test_ServeAndWait_FailedBecauseOfNoHandler(t *testing.T) {
	assert := assert.New(t)

	httpServer := NewHttpServer()
	assert.NotNil(httpServer.ServeAndWait())
	httpServer.SetHttpPort(80)
	assert.NotNil(httpServer.ServeAndWait())
}

func Test_ServeAndWait_FailedBecauseOfNoPort(t *testing.T) {
	assert := assert.New(t)

	httpServer := NewHttpServer()
	httpServer.SetFileServer(".")
	assert.NotNil(httpServer.ServeAndWait())
}
