package appcontext

var appServerContext *v1AppServerContext

func init() {
	appServerContext = newAppContext()
}

func GetAppContext() AppServerContext {
	return appServerContext
}
