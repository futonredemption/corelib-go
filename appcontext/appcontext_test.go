package appcontext

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type BlankService struct {
}

func (this *BlankService) GetServiceName() string {
	return ""
}

type FakeService struct {
}

func (this *FakeService) GetServiceName() string {
	return "fake-service"
}

type ExampleService struct {
}

func (this *ExampleService) GetServiceName() string {
	return "example"
}

func TestNew(t *testing.T) {
	assert := assert.New(t)

	context := newAppContext()
	assert.Nil(context.GetService(""), "\"\" should be nil.")
	assert.Equal(false, context.HasService(""), "Service \"\" should NOT exist.")
}

func TestRegisterBlankService(t *testing.T) {
	assert := assert.New(t)

	context := newAppContext()
	blankService := &BlankService{}

	assert.NotNil(context.RegisterService(blankService), "Blank service should NOT be registered.")
	assert.False(context.HasService(blankService.GetServiceName()), "Blank service should NOT be in registry.")
	assert.Nil(context.GetService(blankService.GetServiceName()), "Blank service should not be gettable.")
}

func TestRegisterGetService(t *testing.T) {
	assert := assert.New(t)

	context := newAppContext()
	fakeService := &FakeService{}

	assert.Nil(context.RegisterService(fakeService), "fake-service should be registered.")
	assert.True(context.HasService("fake-service"), "fake-service should exist.")
	assert.Equal(fakeService, context.GetService("fake-service"), "fake-service instances should match.")
}

func TestDoubleRegister_SecondFails(t *testing.T) {
	assert := assert.New(t)

	context := newAppContext()
	fakeService := &FakeService{}
	assert.Nil(context.RegisterService(fakeService), "fake-service should be registered.")
	assert.NotNil(context.RegisterService(fakeService), "Reregister fake-service should fail.")
}

func ExampleRegisterService() {
	GetAppContext().RegisterService(&ExampleService{})
	service := GetAppContext().GetService("example")
	fmt.Println(service.GetServiceName())
	// Output: example
}
