package appcontext

import (
	"errors"
	"fmt"
)

type AppService interface {
	GetServiceName() string
}

type AppServerContext interface {
	RegisterService(service AppService) error
	HasService(name string) bool
	GetService(name string) AppService
}

type v1AppServerContext struct {
	services map[string]AppService
}

func (this *v1AppServerContext) RegisterService(service AppService) error {
	serviceName := service.GetServiceName()
	if serviceName == "" {
		return errors.New("Service name cannot be null or empty.")
	}

	if this.HasService(serviceName) {
		return fmt.Errorf("Cannot register service, %s, it has already been registered. ", service)
	}
	this.services[service.GetServiceName()] = service
	return nil
}

func (this *v1AppServerContext) HasService(name string) bool {
	return this.services[name] != nil
}

func (this *v1AppServerContext) GetService(name string) AppService {
	return this.services[name]
}

func newAppContext() *v1AppServerContext {
	return &v1AppServerContext{
		services: map[string]AppService{},
	}
}
