// Package to manage configuration of an application.
package config

// Interface for interacting with a certain configuration.
type Config interface {
	// Loads the configuration, synchronously.
	Load() error
	// Saves the configuration, synchronously.
	Save() error
	// Gets a configuration value.
	GetString(name string) string
}
