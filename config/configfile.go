package config

import (
	"encoding/json"
	"io/ioutil"
	"os"
)

type v1JsonConfigFile struct {
	configFile string
	configData interface{}
}

func (this *v1JsonConfigFile) Load() error {
	contents, err := ioutil.ReadFile(this.configFile)
	if os.IsNotExist(err) {
		return this.loadConfig([]byte("{}"))
	} else if err != nil {
		return err
	}
	return this.loadConfig(contents)
}

func (this *v1JsonConfigFile) Save() error {
	content, err := this.getSerialized()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(this.configFile, content, 0660)
}

func (this *v1JsonConfigFile) loadConfig(contents []byte) error {
	return json.Unmarshal(contents, &this.configData)
}

func (this *v1JsonConfigFile) getSerialized() ([]byte, error) {
	return json.MarshalIndent(this.configData, "", "  ")
}

func NewJsonConfigFile(configFile string, emptyData interface{}) *v1JsonConfigFile {
	return &v1JsonConfigFile{
		configFile: configFile,
		configData: emptyData,
	}
}
