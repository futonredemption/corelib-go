package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type ExampleConfigData struct {
	WorkspacePath string
}

func TestLoad_Empty(t *testing.T) {
	assert := assert.New(t)

	config := NewJsonConfigFile("config.json", &ExampleConfigData{})
	assert.Nil(config.Load(), "Should not avail if config does not exist.")
}

func TestGetSerialized_Empty(t *testing.T) {
	assert := assert.New(t)

	initialData := &ExampleConfigData{
		WorkspacePath: "workspace",
	}

	config := NewJsonConfigFile("config.json", initialData)

	assert.Equal("workspace", initialData.WorkspacePath, "initialData.WorkspacePath")
	data, err := config.getSerialized()
	assert.Nil(err, "config.getSerialized() error should be nil.")
	assert.Equal("{\n  \"WorkspacePath\": \"workspace\"\n}", string(data), "config.getSerialized()")
}

func TestGetSerial(t *testing.T) {
	assert := assert.New(t)

	initialData := &ExampleConfigData{
		WorkspacePath: "workspace",
	}

	config := NewJsonConfigFile("config.json", initialData)

	assert.Equal("workspace", initialData.WorkspacePath, "initialData.WorkspacePath")
	config.loadConfig([]byte("{ \"WorkspacePath\": \"garbage\" }"))
	data, err := config.getSerialized()
	assert.Nil(err, "config.getSerialized() error should be nil.")
	assert.Equal(initialData.WorkspacePath, "garbage", "initialData.WorkspacePath")
	assert.Equal("{\n  \"WorkspacePath\": \"garbage\"\n}", string(data), "config.getSerialized()")
}
